#!/usr/bin/env python3
from pathlib import Path
from pytube import YouTube
from gi.repository import GLib
import os
def main():
    destination = Path(GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_DOWNLOAD))
    link = input("Put the link here:")
    yt = YouTube(link)
    convert = input("Do you want download this video as an audio")
    if convert == "yes":
        video = yt.streams.filter(only_audio=True).first() 
# download the file
        out_file = video.download(output_path=destination)
# save the file
        base, ext = os.path.splitext(out_file)
        new_file = base + '.mp3'
        os.rename(out_file, new_file)
# result of success
        print("The audio's "+ yt.title + " has been dowloaded")
    elif convert == "no":
        ys = yt.streams.get_highest_resolution()
        print("Downloading... " + yt.title)
        ys.download(destination)

    
    



if __name__ == "__main__":
    main()
